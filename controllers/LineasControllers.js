const { sqlConfig, sqlConfigERP } = require("../conexion/index");
const moment = require("moment");
let resultado = [];

async function CabeceraPedidos(req, res) {
  let FechaInicio = "2021-10-10";
  let FechaFinal = moment().format("YYYY-MM-DD");

  const result = await sqlConfig.executeQuery(
    "select * from CabeceraPedidoCliente where estado=0 and FechaPedido between @fechaInicio and @fechaFin ",
    [
      { name: "fechaInicio", type: "varchar", value: FechaInicio },
      { name: "fechaFin", type: "varchar", value: FechaFinal },
    ]
  );
  const resultERP = await sqlConfigERP.executeQuery(
    "SELECT Count(*) AS numero FROM sys_document_groups where group_status=0 "
  );

  res.render("listado.pug", {
    FechaInicio,
    FechaFinal,
    resultado: result.data[0],
    resultadoERP: resultERP.data[0],
    moment,
  });
}

async function LineasPedidos(req, res) {
  const codigo = req.params.codigocliente;
  const numeroPedido = req.params.numero;
  const serie = req.params.serie;
  const ejercicio = req.params.ejercicio;
  const todo = req.params.todo;
  let result = [];

  if (todo == undefined) {
    const resulta = await sqlConfig.executeQuery(
      "SELECT * FROM CabeceraPedidoCliente where CodigoCliente=@codigo and estado=0",
      [{ name: "codigo", type: "varchar", value: codigo }]
    );

    res.render("lineasPedido", {
      resultado: resulta.data[0].length,
    });
  }

  if (todo == "True") {
    result = await sqlConfig.executeQuery(
      "SELECT * FROM LineasPedidoCliente " +
        " WHERE NumeroPedido = ANY (" +
        "SELECT Documento" +
        " FROM MovimientoPendientes m" +
        " JOIN Articulos AS a" +
        " ON m.CodigoArticulo = a.CodigoArticulo " +
        " WHERE(CodigoCliente=@codigo AND OrigenMovimiento=@origen AND periodo=5)" +
        " GROUP BY Documento)",
      [
        { name: "codigo", type: "varchar", value: codigo },
        { name: "origen", type: "varchar", value: "C" },
      ]
    );
    resultado = result.data[0];
    module.exports = {
      lineas: resultado,
      sqlConfig,
      sqlConfigERP,
    };
    //require("../prueba");

    res.render("lineasPedido", {
      resultado,
    });
  } else if (todo == "False") {
    result = await sqlConfig.executeQuery(
      "SELECT * FROM LineasPedidoCliente" +
        "  WHERE(EjercicioPedido=@ejercicio AND NumeroPedido=@numero AND SeriePedido=@serie)",
      [
        { name: "numero", type: "Int", value: numeroPedido },
        { name: "serie", type: "varchar", value: serie },
        { name: "ejercicio", type: "varchar", value: ejercicio },
      ]
    );
    //interrumpir flujo para poder modificar la salida
    resultado = result.data[0];
    module.exports = {
      lineas: resultado,
      sqlConfig,
      sqlConfigERP,
    };
    //new require("../prueba");

    res.render("lineasPedido", {
      resultado,
    });
  }
}

async function GrupoPedido(req, res) {
  const result = await sqlConfigERP.executeQuery(
    "SELECT * FROM sys_document_groups where group_status=0 order by group_number asc"
  );

  resultado = result.data[0];

  res.render("grupos", {
    resultado,
    moment,
  });
}

module.exports = {
  CabeceraPedidos,
  LineasPedidos,
  GrupoPedido,
};
