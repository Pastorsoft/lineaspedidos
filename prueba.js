const config = require('./config')
const { lineas, sqlConfig, sqlConfigERP } = require('./controllers/LineasControllers')


//Cálculo para el preprocesado de Picking
//Variables disponibles:
//Servidor, BaseDatos, Usuario, Password
//ERPServidor, ERPBaseDatos, ERPUsuario, ERPPassword
//CodigoEmpresa---->preguntar de donde viene

//TablaArticulosPicking: lista de artículos del Picking. Columnas: IdElemento, CodigoArticulo, UnidadMedida, CodigoColor, NumeroTalla, Partida, Cantidad
//TablaReferenciasDocumento: lista de referencias de documento en los que está cada artículo del Picking. Columnas: IdElemento, IdLinea, IdLineaTallaColor, Cantidad, ExcluirPicking

//Resultado:
//Se permitirá la modificación de la columna Cantidad de TablaReferenciasDocumento
//Para excluir registros, será necesario establecer la columna ExcluirPicking a True en TablaReferenciasDocumento
//TablaArticulosPicking es sólo de consulta y se actualizará automáticamente en función de los registros modificados en TablaReferenciasDocumento
//También está disponible la variable \"Proceder\" de tipo booleano para definir si se procede con la gestión del Picking
//También está disponible la variable \"Mensaje\" de tipo string, para mostrar un mensaje por pantalla, independientemente de si se procede o no con la gestión del Picking. Este mensaje estará disponible para su consulta durante todo el proceso de picking
//También está disponible la variable \"CentroLogistico\" de tipo string, para indicar el guid del centro logístico sobre el que calcular la ruta. En caso de no informarlo se cogerá el estándar



// Variables result que son las lineas pedido
//CodigoArticulo,CodigoAlmacen,DescripcionArticulo,Unidades,EjercicioPedido
function LlenadoTabla(){
    let reg = lineas.length
    let num = 0;
    let TablaReferenciasDocumento=[]

    while (num < reg) {
        TablaReferenciasDocumento.push({
            IdElemento:1,
            IdLinea:lineas[num].LineasPosicion,
            IdLineaTallaColor:num,
            Cantidad:lineas[num].UnidadesPedidas,
            ExcluirPicking:false
        })
        num++;
    }
    console.log(TablaReferenciasDocumento);
}


async function Preprocesado() {
    
    let reg = lineas.length
    let num = 0;
   
    while (num < reg) {
        //console.log(lineas[num1]);
        // lineas[num].Unidades= lineas[num].Unidades+5
        //lineas[num].Unidades =  sqlConfig.executeQuery('Update LineasPedidoCliente set UnidadesPedidas = UnidadesPedidas - 3 where numeroPedido= 5 ')
        const li = await sqlConfigERP.executeQuery('Select * from mst_users')
        //console.log(li.data[0]);
        num= num + 1;
    }
    
}
LlenadoTabla()
Preprocesado()












