const express = require("express");

const Controllers = require("../controllers/LineasControllers");

const router = express.Router();

router.get("/", Controllers.CabeceraPedidos);

router.get(
  "/pedido/:numero/:serie/:ejercicio/:codigocliente/:todo?",
  Controllers.LineasPedidos
);

router.get("/grupos", Controllers.GrupoPedido);

module.exports = router;
