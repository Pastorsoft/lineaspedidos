const express = require("express");
const app = express();
const port = 3000;

const rutas = require("./Routes/Rutas");

app.set("views", "./Views");

app.set("view engine", "pug");

app.use(express.static(__dirname + "/Public"));

app.use(express.json());

app.use("/", rutas);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
